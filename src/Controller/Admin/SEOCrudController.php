<?php

namespace App\Controller\Admin;

use App\Entity\SEO;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SEOCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SEO::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Meta Information");
        yield TextField::new("heading")->setLabel("Website Title");
        yield TextField::new("meta_title")->setLabel("SERP Title");
        yield TextField::new("meta_description")->setLabel("SERP Description")
            ->onlyOnForms();
        yield TextField::new("keywords")->setLabel("Keywords")
            ->onlyOnForms();
        yield ImageField::new("logo_path")->setLabel("Logo")
            ->setUploadDir("/public/site/images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images");
    }

}
