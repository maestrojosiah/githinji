-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: githinji
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `publication_date` datetime DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `author_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C0155143F675F31B` (`author_id`),
  CONSTRAINT `FK_C0155143F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'Maneuvering the Divorce Process in Kenya: A Comprehensive Guide','<p>Divorce is a significant life event that can be emotionally challenging and legally complex. In Kenya, navigating the divorce process requires careful consideration of legal procedures and personal circumstances. This article aims to provide a comprehensive guide to maneuvering the divorce process in Kenya, outlining the steps involved, legal requirements, and practical tips for individuals considering or going through a divorce.</p>\r\n\r\n<p>Understanding Divorce Laws in Kenya: In Kenya, the law governing divorce is primarily found in the Marriage Act and the Matrimonial Property Act. These laws outline the grounds for divorce, procedures for filing a divorce petition, and the division of matrimonial property.</p>\r\n\r\n<p>Grounds for Divorce: Under Kenyan law, there are several grounds upon which a spouse can file for divorce. These grounds include:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Adultery: If one spouse engages in extramarital affairs, the other spouse may file for divorce on the grounds of adultery.</p>\r\n	</li>\r\n	<li>\r\n	<p>Desertion: If one spouse abandons the other for a continuous period of at least three years without consent or justifiable reason, the abandoned spouse may file for divorce.</p>\r\n	</li>\r\n	<li>\r\n	<p>Cruelty: If one spouse subjects the other to physical or mental cruelty, causing reasonable apprehension of danger to life, limb, or health, the aggrieved spouse may file for divorce on the grounds of cruelty.</p>\r\n	</li>\r\n	<li>\r\n	<p>Irretrievable Breakdown of Marriage: If the marriage has irretrievably broken down, and there is no reasonable prospect of reconciliation, either spouse may petition for divorce.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Filing for Divorce: To initiate the divorce process in Kenya, one spouse (the petitioner) must file a divorce petition in the High Court. The petition should specify the grounds for divorce and any claims regarding custody of children, maintenance, or division of property.</p>\r\n\r\n<p>Service of Divorce Petition: Once the divorce petition is filed, the petitioner must serve a copy of the petition to the other spouse (the respondent). The respondent then has an opportunity to respond to the allegations raised in the petition.</p>\r\n\r\n<p>Mediation and Counseling: In some cases, the court may encourage mediation or counseling to facilitate reconciliation between the parties. If reconciliation efforts fail, the divorce proceedings will continue.</p>\r\n\r\n<p>Division of Matrimonial Property: During divorce proceedings, the court will also consider the division of matrimonial property. Matrimonial property includes assets acquired during the marriage, such as real estate, vehicles, investments, and household goods. The court aims to achieve a fair and equitable distribution of matrimonial property, taking into account the contributions of each spouse to the marriage.</p>\r\n\r\n<p>Child Custody and Maintenance: In cases where the divorcing couple has children, the court will prioritize the best interests of the children when determining custody and maintenance arrangements. The court may grant joint custody or sole custody to one parent, taking into account factors such as the child&#39;s age, welfare, and parental capacity.</p>\r\n\r\n<p>Navigating the divorce process in Kenya requires careful consideration of legal requirements, personal circumstances, and the well-being of all parties involved. By understanding the grounds for divorce, filing procedures, and legal considerations regarding property division and child custody, individuals can navigate the divorce process with clarity and confidence. Seeking guidance from experienced legal professionals can also provide invaluable support and assistance throughout the divorce proceedings. Remember, divorce is a challenging journey, but with the right resources and support, individuals can emerge stronger and move forward towards a new chapter in their lives.</p>','2024-03-04 05:31:20',1,'divorce-process-kenya','divorce-kenya-1709530280.jpg','The Divoce Process In Kenya - The Complete Guide','This article aims to provide a comprehensive guide to maneuvering the divorce process in Kenya, outlining the steps involved, legal requirements, practical tips',1),(2,'Resolving Disputes Effectively: Strategies for Success in Kenya','<p>Disputes are an inevitable part of life, whether in business, family, or other relationships. In Kenya, effective dispute resolution is crucial for maintaining harmony, protecting rights, and avoiding prolonged legal battles. This article explores various strategies and methods for resolving disputes effectively in Kenya, empowering individuals and organizations to navigate conflicts with confidence and efficiency.</p>\r\n\r\n<p>Understanding Dispute Resolution Options: In Kenya, there are several avenues for resolving disputes, each with its own procedures and benefits. These include negotiation, mediation, arbitration, and litigation.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Negotiation: Negotiation involves direct communication between parties to reach a mutually acceptable solution. It can be informal or facilitated by legal representatives. Negotiation allows parties to maintain control over the outcome and preserve relationships.</p>\r\n	</li>\r\n	<li>\r\n	<p>Mediation: Mediation is a voluntary and confidential process facilitated by a neutral third party (the mediator). The mediator helps parties identify issues, explore interests, and generate options for resolution. Mediation empowers parties to find creative solutions while preserving relationships and avoiding the adversarial nature of litigation.</p>\r\n	</li>\r\n	<li>\r\n	<p>Arbitration: Arbitration involves submitting the dispute to a neutral third party (the arbitrator) who renders a binding decision based on evidence and arguments presented by the parties. Arbitration offers a faster and more flexible alternative to litigation, with proceedings conducted in private and tailored to the parties&#39; needs.</p>\r\n	</li>\r\n	<li>\r\n	<p>Litigation: Litigation involves resolving disputes through the court system, with a judge or magistrate making a final decision based on evidence and legal arguments presented by the parties. While litigation can be time-consuming and costly, it provides a formal process for enforcing rights and obtaining legal remedies.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Choosing the Right Approach: When faced with a dispute, choosing the right approach to resolution is crucial. Factors to consider include the nature of the dispute, the preferences of the parties, time constraints, and the desired outcome. Collaborative approaches such as negotiation and mediation are often preferred for preserving relationships and achieving mutually beneficial outcomes. However, in cases where parties cannot reach a voluntary agreement, arbitration or litigation may be necessary to resolve the dispute conclusively.</p>\r\n\r\n<p>Key Strategies for Effective Dispute Resolution: Regardless of the chosen approach, several key strategies can enhance the effectiveness of dispute resolution in Kenya:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Communication: Open and respectful communication is essential for understanding each party&#39;s interests, concerns, and perspectives.</p>\r\n	</li>\r\n	<li>\r\n	<p>Preparation: Adequate preparation, including gathering relevant information and understanding legal rights and obligations, enhances parties&#39; ability to negotiate or present their case effectively.</p>\r\n	</li>\r\n	<li>\r\n	<p>Flexibility: Remaining flexible and open to compromise allows parties to explore creative solutions and find common ground.</p>\r\n	</li>\r\n	<li>\r\n	<p>Professional Guidance: Seeking guidance from experienced legal professionals or mediators can provide invaluable support and expertise throughout the dispute resolution process.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Effective dispute resolution is essential for maintaining peace, fostering collaboration, and protecting rights in Kenya. By understanding the available options, choosing the right approach, and employing key strategies for success, individuals and organizations can navigate conflicts with confidence and achieve satisfactory outcomes. Whether through negotiation, mediation, arbitration, or litigation, resolving disputes effectively empowers parties to move forward positively and constructively.</p>','2024-03-04 08:17:23',1,'dispute-resolution-kenya','dispute-resolution-1709540243.jpg','Dispute Resolution in Kenya - Strategies for Success','This article explores various strategies and methods for resolving disputes effectively in Kenya, empowering individuals and organizations with efficiency.',1),(3,'Navigating Corporate Legal Challenges: Insights for Businesses in Kenya','<p>In today&#39;s dynamic business environment, companies in Kenya face a myriad of legal challenges that can impact their operations, growth, and reputation. From compliance issues to contract disputes, navigating corporate legal challenges requires proactive measures and strategic planning. This article aims to provide valuable insights and practical tips for businesses in Kenya to effectively navigate common corporate legal challenges and mitigate risks.</p>\r\n\r\n<p>Understanding Corporate Legal Challenges: Corporate legal challenges encompass a wide range of issues that businesses may encounter in their day-to-day operations. These challenges may include:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Compliance: Ensuring compliance with local laws, regulations, and industry standards is essential for avoiding legal penalties and reputational damage.</p>\r\n	</li>\r\n	<li>\r\n	<p>Contract Management: Drafting, reviewing, and enforcing contracts with customers, suppliers, and business partners requires careful attention to detail and legal expertise.</p>\r\n	</li>\r\n	<li>\r\n	<p>Intellectual Property Protection: Safeguarding intellectual property rights, such as trademarks, copyrights, and patents, is crucial for protecting valuable assets and maintaining competitive advantage.</p>\r\n	</li>\r\n	<li>\r\n	<p>Employment Law: Managing employee relations, drafting employment contracts, and addressing workplace disputes in compliance with labor laws and regulations.</p>\r\n	</li>\r\n	<li>\r\n	<p>Corporate Governance: Establishing effective corporate governance structures and practices to promote transparency, accountability, and ethical conduct within the organization.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Strategies for Navigating Corporate Legal Challenges: To navigate corporate legal challenges effectively, businesses in Kenya can adopt the following strategies:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Proactive Risk Management: Conducting regular risk assessments to identify potential legal risks and implementing measures to mitigate or avoid them before they escalate.</p>\r\n	</li>\r\n	<li>\r\n	<p>Legal Compliance Programs: Developing comprehensive compliance programs tailored to the specific regulatory requirements and industry standards applicable to the business.</p>\r\n	</li>\r\n	<li>\r\n	<p>Legal Due Diligence: Conducting thorough legal due diligence before entering into new business ventures, mergers, acquisitions, or partnerships to identify potential legal issues and liabilities.</p>\r\n	</li>\r\n	<li>\r\n	<p>Engaging Legal Counsel: Seeking advice and guidance from experienced corporate lawyers or legal advisors who can provide proactive legal support and strategic advice.</p>\r\n	</li>\r\n	<li>\r\n	<p>Training and Awareness: Providing ongoing training and awareness programs for employees to ensure they understand their legal obligations and responsibilities.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Case Studies and Practical Tips: This article could include case studies or real-life examples of businesses in Kenya successfully navigating corporate legal challenges. It could also offer practical tips and best practices based on these experiences, such as:</p>\r\n\r\n<ul>\r\n	<li>Implementing robust compliance policies and procedures.</li>\r\n	<li>Conducting regular legal audits and reviews.</li>\r\n	<li>Establishing clear communication channels with legal counsel.</li>\r\n	<li>Staying informed about changes in laws and regulations affecting the business.</li>\r\n</ul>\r\n\r\n<p>Navigating corporate legal challenges is an integral part of running a successful business in Kenya. By understanding the nature of these challenges, adopting proactive strategies, and seeking timely legal advice, businesses can minimize risks, protect their interests, and focus on achieving their strategic objectives. With careful planning and diligence, businesses can navigate the complexities of the legal landscape and thrive in the competitive market environment.</p>','2024-03-04 08:23:44',1,'navigating-corporate-challenges-kenya','corporate-challenges-1709540624.jpg','How To Navigate Legal Challenges In Kenya','Find in this article valuable insights and practical tips for businesses in Kenya to effectively navigate common corporate legal challenges and mitigate risks.',1);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,'Law','Blog related to general law');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category_blog`
--

DROP TABLE IF EXISTS `blog_category_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `blog_category_blog` (
  `blog_category_id` int NOT NULL,
  `blog_id` int NOT NULL,
  PRIMARY KEY (`blog_category_id`,`blog_id`),
  KEY `IDX_3808E168CB76011C` (`blog_category_id`),
  KEY `IDX_3808E168DAE07E97` (`blog_id`),
  CONSTRAINT `FK_3808E168CB76011C` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_3808E168DAE07E97` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category_blog`
--

LOCK TABLES `blog_category_blog` WRITE;
/*!40000 ALTER TABLE `blog_category_blog` DISABLE KEYS */;
INSERT INTO `blog_category_blog` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `blog_category_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_text` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,'Company Name','company-name','Kimotho Githinji Co & Advocates'),(2,'Main H1 homepage','homepage-main-h1','Law Firm In Nairobi Kenya'),(3,'Main H2 homepage','homepage-main-h2','We Solve Legal Problems'),(4,'Main Slide Text','main-slide-text','At our firm, we pride ourselves on our wide-ranging expertise and a steadfast commitment to providing top-notch services to both local and global clientele.'),(5,'Second Slider Text','second-slider-text','Our seasoned team is dedicated to delivering personalized representation and customized solutions, ensuring an unparalleled experience for each client we serve.'),(6,'Action Button For Sliders','action-button-slider','Free Consultation'),(7,'About Us Heading','about-us-heading','About the Best Law Advocates Nairobi'),(8,'About Us Text','about-us-text','Here at Kimotho Githinji & Company Advocates, we pride ourselves on being a customer centric law firm dedicated to providing comprehensive legal services in Kenya and around the world. We are a broad-based practice with a reputation for offering excellent quality services to our domestic and international clients. With a background of experienced and dedicated staff, we are committed to delivering exceptional individualized representation and tailored solutions to our clients. Here, you can be assured of honest, trustworthy and professional services with integrity as our back bone. We cushion our clients by offering preventive legal support that foresees challenges ahead and makes legally sound provision for the same.'),(9,'About Us Action','about-us-action','Discover More'),(10,'Why Choose Us Title','why-us-title','Why We Are Different From Others'),(11,'Why Us Top Left Title','why-us-top-left-title','Experts in Law'),(12,'Why Us Top Right Title','why-us-top-right-title','Client-Centric Approach'),(13,'Why Us Bottom Left Title','why-us-bottom-left-title','Ethical Standards'),(14,'Why Us Bottom Right Title','why-us-bottom-right-title','Results-Driven'),(15,'Why Us Top Left Text','why-us-top-left-text','Our team comprises seasoned legal professionals with in-depth knowledge and expertise in diverse practice areas.'),(16,'Why Us Top Right Text','why-us-top-right-text','We prioritize our clients\' needs, offering personalized attention and customized legal solutions tailored to their specific circumstances.'),(17,'Why Us Bottom Left Text','why-us-bottom-left-text','We uphold the highest ethical standards, honesty, integrity, efficiency and professionalism, we strive to earn our clients\' trust and confidence. We are trustworthy and consistent.'),(18,'Why Us Bottom Right Text','why-us-bottom-right-text','Outcomes matter. We are committed to achieving favorable outcomes for our clients, protecting their rights and interests, giving them much needed confidence and peace of mind.'),(19,'Service Section Title','service-section-title','Our Practice Areas'),(20,'Call To Action Text','call-to-action-text','We are always ready for you'),(21,'Call To Action Solution','call-to-action-solution','+254 768 105556'),(22,'Team Section Title','team-title','Meet Our Expert Team'),(23,'Testimonial Title','testimonial-title','What they\'re saying about us'),(24,'Frequently Asked Questions Title','faq-title','Frequently Asked Questions'),(25,'Consultation Title','consultation-title','Get a free Consultation');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `answer` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'What services does Kimotho Githinji & Company Advocates offer?','<div>Kimotho Githinji &amp; Company Advocates provides comprehensive legal services in various practice areas including corporate law, commercial law, real estate &amp; conveyancing law, family law, dispute resolution, employment and labour relations law, and energy, construction, and engineering law.</div>'),(2,'Where is Kimotho Githinji & Company Advocates located?','<div>Our office is conveniently located in the serene Kilimani neighborhood at the Hurlingham Shopping Centre in Nairobi, Kenya.</div>'),(3,'How can I contact Kimotho Githinji & Company Advocates?','<div>You can reach us by visiting our office at the Hurlingham Shopping Centre, Nairobi, Kilimani area. Additionally, you can contact us via phone or email for trusted legal representation and advice.<br><br>Kimotho Githinji &amp; Company Advocates,&nbsp;</div><div>Advocates, Commissioners for Oaths and Notary Public.</div><div><br></div><div><strong>Physical Address:</strong> Argwings Kodhek Groove, (next to Markson Suites),</div><div>Balala Maisonettes, #2&nbsp;</div><div>Hurlingham Shopping Centre, Kilimani, Nairobi, Kenya (Parking available).</div><div><br></div><div><strong>Postal Address:</strong> Box 28386-00200, City Square, Nairobi</div><div><br></div><div><strong>Phone Number:</strong> <strong>0768 105556</strong></div>'),(4,'What sets Kimotho Githinji & Company Advocates apart from other law firms?','<div>Our firm distinguishes itself through expertise, a client-centric approach, ethical standards, and a results-driven mentality. We prioritize our clients\' needs, uphold the highest ethical standards, and strive for favorable outcomes while maintaining trust and confidence.</div>'),(5,'Can Kimotho Githinji & Company Advocates handle international legal matters?','<div>Yes, we have experience in providing legal services not only in Kenya but also internationally. Our team is equipped to handle diverse legal matters across borders.</div>'),(6,'How does Kimotho Githinji & Company Advocates ensure client satisfaction?','<div>We prioritize client satisfaction by offering personalized attention, customized legal solutions, and a commitment to achieving favorable outcomes. Our team of seasoned legal professionals works diligently to protect our clients\' rights and interests.</div>'),(7,'Does Kimotho Githinji & Company Advocates offer preventive legal support?','<div>Yes, we provide preventive legal support that anticipates challenges and makes legally sound provisions to mitigate risks for our clients. We offer proactive guidance to navigate potential legal issues effectively.</div>'),(8,'What types of clients does Kimotho Githinji & Company Advocates serve?','<div>Our client base includes both domestic and international clients, ranging from individuals to corporations. We cater to clients with diverse legal needs and strive to provide tailored solutions for each client.</div>');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'Slider Image','slider-image','law-firm-1-1709469350.jpg'),(2,'About Us Image','about-us','law-firm-2-1709469516.jpg'),(3,'Why Us Image','why-us','law-docs-1709469725.jpg'),(4,'FAQ Image','faq','law-faq-1709470060.jpg'),(5,'Consult Us Image','consult-us','consult-us-1709470287.jpg');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` longtext,
  `received_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext NOT NULL,
  `headers` longtext NOT NULL,
  `queue_name` varchar(190) NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messenger_messages`
--

LOCK TABLES `messenger_messages` WRITE;
/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
INSERT INTO `messenger_messages` VALUES (1,'O:36:\\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\\":2:{s:44:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\\";a:1:{s:46:\\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\\";a:1:{i:0;O:46:\\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\\":1:{s:55:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\\";s:21:\\\"messenger.bus.default\\\";}}}s:45:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message\\\";O:51:\\\"Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\\":2:{s:60:\\\"\\0Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\0message\\\";O:39:\\\"Symfony\\\\Bridge\\\\Twig\\\\Mime\\\\TemplatedEmail\\\":5:{i:0;s:30:\\\"reset_password/email.html.twig\\\";i:1;N;i:2;a:1:{s:10:\\\"resetToken\\\";O:58:\\\"SymfonyCasts\\\\Bundle\\\\ResetPassword\\\\Model\\\\ResetPasswordToken\\\":4:{s:65:\\\"\\0SymfonyCasts\\\\Bundle\\\\ResetPassword\\\\Model\\\\ResetPasswordToken\\0token\\\";s:40:\\\"XcgAqYORbMH0fA1FOyrr65782zuuZPDmLQEmycWm\\\";s:69:\\\"\\0SymfonyCasts\\\\Bundle\\\\ResetPassword\\\\Model\\\\ResetPasswordToken\\0expiresAt\\\";O:17:\\\"DateTimeImmutable\\\":3:{s:4:\\\"date\\\";s:26:\\\"2024-03-03 07:14:12.003949\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:3:\\\"UTC\\\";}s:71:\\\"\\0SymfonyCasts\\\\Bundle\\\\ResetPassword\\\\Model\\\\ResetPasswordToken\\0generatedAt\\\";i:1709446452;s:73:\\\"\\0SymfonyCasts\\\\Bundle\\\\ResetPassword\\\\Model\\\\ResetPasswordToken\\0transInterval\\\";i:1;}}i:3;a:6:{i:0;N;i:1;N;i:2;N;i:3;N;i:4;a:0:{}i:5;a:2:{i:0;O:37:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\\":2:{s:46:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\0headers\\\";a:3:{s:4:\\\"from\\\";a:1:{i:0;O:47:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:4:\\\"From\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:58:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\0addresses\\\";a:1:{i:0;O:30:\\\"Symfony\\\\Component\\\\Mime\\\\Address\\\":2:{s:39:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0address\\\";s:29:\\\"info@kimothogithinjilaw.co.ke\\\";s:36:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0name\\\";s:31:\\\"Kimotho Githinji & Co Advocates\\\";}}}}s:2:\\\"to\\\";a:1:{i:0;O:47:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:2:\\\"To\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:58:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\0addresses\\\";a:1:{i:0;O:30:\\\"Symfony\\\\Component\\\\Mime\\\\Address\\\":2:{s:39:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0address\\\";s:23:\\\"maestrojosiah@gmail.com\\\";s:36:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0name\\\";s:0:\\\"\\\";}}}}s:7:\\\"subject\\\";a:1:{i:0;O:48:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\UnstructuredHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:7:\\\"Subject\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:55:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\UnstructuredHeader\\0value\\\";s:27:\\\"Your password reset request\\\";}}}s:49:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\0lineLength\\\";i:76;}i:1;N;}}i:4;N;}s:61:\\\"\\0Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\0envelope\\\";N;}}','[]','default','2024-03-03 06:14:12','2024-03-03 06:14:12',NULL);
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `page_content` longtext,
  `content_first` tinyint(1) DEFAULT NULL,
  `is_service` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'Corporate Law','Corporate Law','We offer expert legal advice and assistance on corporate matters including business formation and structuring, joint ventures, mergers and acquisitions, corporate governance, and compliance issues.','corporate-law','corporate-law-1709472016.jpg','<p style=\"text-align:left\">We offer expert legal advice and assistance on corporate matters including business formation and structuring, joint ventures, mergers and acquisitions, corporate governance, and compliance issues.</p>',NULL,1),(2,'Commercial Law','Commercial Law','we handle small to large commercial transactions, from affidavits, simple day to day contracts, to more complex contract drafting and negotiation, intellectual property rights, and dispute resolution,','commercial-law','law-firm-2-1709473361.jpg','<p style=\"text-align:left\">we handle small to large commercial transactions, from affidavits, simple day to day contracts, to more complex contract drafting and negotiation, intellectual property rights, and dispute resolution, aviation, immigration law as well as banking finance and securities law.</p>',0,1),(3,'Real Estate & Conveyancing Law','Real Estate & Conveyancing Law','Looking to buy, sell, or lease property, we provide strategic proactive guidance through every step of the process, ensuring smooth transactions and resolving disputes efficiently.','real-estate-conveyancing-law','law-firm-3-1709473435.jpg','<p>Looking to buy, sell, or lease property, we provide strategic proactive guidance through every step of the process, ensuring smooth transactions and resolving disputes efficiently.</p>',0,1),(4,'Family Law','Family Law','We understand the sensitivity and complexity of family matters. Our compassionate family law experts offer personalized guidance and representation','family-law','law-firm-4-1709473479.jpg','<p style=\"text-align:left\">We understand the sensitivity and complexity of family matters. Our compassionate family law experts offer personalized guidance and representation in matters including Family trusts, probate, succession and inheritance, child custody and maintenance, as well as adoption matters.</p>',0,1),(5,'Dispute Resolution','Dispute Resolution','With extensive courtroom experience, our litigators vigorously represent clients in civil and commercial disputes, striving for favorable outcomes through negotiation,  mediation and arbitration.','dispute-resolution','law-firm-5-1709473521.jpg','<p style=\"text-align:left\">With extensive courtroom experience, our litigators vigorously represent clients in civil and commercial disputes, striving for favorable outcomes through negotiation, mediation and arbitration.</p>',0,1),(6,'Employment and Labour relations Law','Employment and Labour relations Law','From drafting employment contracts to resolving workplace disputes, our employment lawyers assist both employers and employees in navigating the intricacies of labor laws and regulations.','employment-labour-relations','law-firm-7-1709473602.jpg','<p style=\"text-align:left\">From drafting employment contracts to resolving workplace disputes, our employment lawyers assist both employers and employees in navigating the intricacies of labor laws and regulations.</p>',0,1),(7,'Energy Law: Natural Resources, Oil and Gas Transactions','Natural Resources, Oil and Gas Transactions - Energy','We have a strong background in energy matters, as well as large scale energy projects construction and related infrastructure.','energy-natural-resources-oil-gas','law-photos-1709555139.jpg','<p style=\"text-align:left\">Welcome to our Energy Law section, focusing on Natural Resources, Oil and Gas Transactions. Our law firm is committed to providing comprehensive legal services tailored to the unique needs of clients involved in the dynamic energy industry. With expertise in power plants, energy resource development, oil and gas transactions, we offer strategic counsel and representation to navigate the complex legal landscape of energy resource development and utilization.</p>\r\n\r\n<p>Services Offered include:</p>\r\n\r\n<h3>1. Title Examination and Due Diligence:</h3>\r\n\r\n<p>Our team conducts thorough title examinations and due diligence to ensure clear and marketable title ownership for oil and gas properties. We meticulously review deeds, leases, contracts, and other relevant documents to identify any potential issues or encumbrances affecting the property.</p>\r\n\r\n<h3>2. Lease Negotiation and Drafting:</h3>\r\n\r\n<p>We assist clients in negotiating and drafting lease agreements for oil and gas exploration, production, and development. We ensure that lease terms are favorable and protect the interests of our clients while complying with regulatory requirements and industry standards.</p>\r\n\r\n<h3>3. Purchase and Sale Transactions:</h3>\r\n\r\n<p>Our firm provides guidance and representation in purchase and sale transactions involving oil and gas assets. We handle all aspects of the transaction, including negotiation, drafting of agreements, due diligence, and closing, to facilitate seamless and efficient transactions for our clients.</p>\r\n\r\n<h3>4. Regulatory Compliance:</h3>\r\n\r\n<p>Staying compliant with regulatory requirements is paramount in the energy industry. We advise clients on regulatory matters pertaining to natural resources, oil and gas exploration, production, transportation, and environmental compliance. We keep abreast of evolving regulations to help clients navigate the regulatory landscape effectively.</p>\r\n\r\n<h3>5. Dispute Resolution:</h3>\r\n\r\n<p>In the event of disputes arising from natural resources, oil and gas transactions, our experienced litigators provide avid representation to protect our clients&#39; interests. We handle disputes related to breach of contract, lease agreements, royalty payments, property rights, and other contentious issues with a focus on achieving favorable outcomes for our clients.</p>\r\n\r\n<h2>Why Choose Us:</h2>\r\n\r\n<p>A. Industry Knowledge and Experience:<br />\r\nWe possess in-depth knowledge and extensive experience in energy and natural resources law, with a focus on natural resources, oil and gas transactions. We understand the complexities of the industry and provide tailored solutions to meet the unique needs of our clients.</p>\r\n\r\n<h3>B. Client-Centric Approach:</h3>\r\n\r\n<p>We prioritize client satisfaction and strive to deliver exceptional service at every stage of representation. We are dedicated to understanding our clients&#39; goals and objectives, providing proactive guidance, and achieving favorable results on their behalf.</p>\r\n\r\n<h3>C. Collaborative Approach:</h3>\r\n\r\n<p>We work closely with clients, industry experts, and other stakeholders to develop strategic solutions and address challenges effectively. Our collaborative approach fosters strong relationships and ensures that our clients receive comprehensive legal support tailored to their specific needs.</p>\r\n\r\n<h3>D. Commitment to Excellence:</h3>\r\n\r\n<p>Our firm is committed to upholding the highest standards of professionalism, integrity, and excellence in legal representation. We leverage our knowledge, skills, and resources to deliver superior outcomes for our clients and exceed their expectations.</p>\r\n\r\n<h2>Reach out!</h2>\r\n\r\n<p>If you require legal assistance or have any inquiries regarding natural resources, oil and gas transactions, we invite you to contact our firm. We are ready to provide personalized guidance and support to help you achieve your objectives in the dynamic energy industry.</p>',0,1),(8,'Law Firm In Nairobi Kenya','Law Firm In Nairobi Kenya','We are a broad-based practice with a reputation for offering excellent quality services to our domestic and international clients.','home-page',NULL,NULL,0,0),(9,'Our Services','Our Professional Services','We offer expert legal advice and assistance on corporate matters including business formation and structuring, joint ventures, mergers and acquisitions, corporate governance, and compliance issues.','services',NULL,NULL,0,0),(10,'Our Pricing','Our Pricing - Kimotho Githinji & Co Advocates','Our pricing is friendly to your pocket, even when our services are professional.','pricing',NULL,'<p>Some info about pricing</p>',0,0),(11,'Frequently Asked Questions','FAQ - Kimotho Githinji & Co Advocates','Frequently Asked Questions about Kimotho Githinji & Co Advocates.','faq',NULL,NULL,0,0),(12,'About Us','About - Kimotho Githinji & Co Advocates','Learn more about Kimotho Githinji & Co Advocates','about',NULL,NULL,0,0),(13,'Blog Articles','Blog Articles - Kimotho Githinji & Co Advocates','We are a broad-based practice with a reputation for offering excellent quality services to our domestic and international clients.','blog-articles',NULL,NULL,0,0),(14,'Contact Us','Our Contacts','Reach out to us today','contact',NULL,NULL,0,0),(15,'Blog Content','Blog Content','Blog Content','blog-content',NULL,NULL,0,0);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_section`
--

DROP TABLE IF EXISTS `page_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `page_section` (
  `page_id` int NOT NULL,
  `section_id` int NOT NULL,
  PRIMARY KEY (`page_id`,`section_id`),
  KEY `IDX_D713917AC4663E4` (`page_id`),
  KEY `IDX_D713917AD823E37A` (`section_id`),
  CONSTRAINT `FK_D713917AC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D713917AD823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_section`
--

LOCK TABLES `page_section` WRITE;
/*!40000 ALTER TABLE `page_section` DISABLE KEYS */;
INSERT INTO `page_section` VALUES (8,1),(8,2),(8,3),(12,2),(12,17);
/*!40000 ALTER TABLE `page_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `reset_password_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `selector` varchar(20) NOT NULL,
  `hashed_token` varchar(100) NOT NULL,
  `requested_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`),
  CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password_request`
--

LOCK TABLES `reset_password_request` WRITE;
/*!40000 ALTER TABLE `reset_password_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_password_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `template_path` varchar(255) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,'Home Page Banner','banner.html.twig',1),(2,'About Us Section','about.html.twig',2),(3,'Services For Home Page','services.html.twig',3),(4,'Why Us Section','why_us.html.twig',5),(5,'Call To Action Banner','cta.html.twig',4),(6,'Team Members For Home Page','team.html.twig',6),(7,'Testimonial Section','testimonial.html.twig',8),(8,'Call To Action Banner 2','cta.html.twig',7),(9,'Frequently Asked Questions','faq.html.twig',9),(10,'Consultation Section','consult.html.twig',10),(11,'Blog Posts Section','blog_posts.html.twig',11),(12,'Call Back Request Section','callback.html.twig',12),(13,'Top Banner For Services Page','services_banner.html.twig',1),(14,'Services For Service Page','services_pg_list.html.twig',2),(15,'Service Page Banner','service_banner.html.twig',1),(16,'Service Details','service.html.twig',2),(17,'Global Banner Section','variable_banner.html.twig',1),(18,'Blog Article Section','blog_article.html.twig',2);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo`
--

DROP TABLE IF EXISTS `seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `seo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `logo_path` varchar(255) DEFAULT NULL,
  `keywords` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo`
--

LOCK TABLES `seo` WRITE;
/*!40000 ALTER TABLE `seo` DISABLE KEYS */;
INSERT INTO `seo` VALUES (1,'Comprehensive Legal Services In Kenya','Comprehensive Legal Support | Kimotho Githinji & Co Advocates','Your trusted professional legal partner in Kenya. Experts in corporate, commercial, real estate, family, dispute resolution & more, get personalized solutions.','kimothogithinjilaw-logo-light-1709299894.png','Law firm Kenya, Legal services Kenya, Customer centric law firm, Experienced lawyers Kenya, International law firm Kenya, Preventive legal services Kenya, Comprehensive legal services, Broad-based legal practice, Individualized representation, Tailored legal solutions, Honest legal services, Trustworthy legal services, Professional legal services, Legal advice Kenya');
/*!40000 ALTER TABLE `seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_member`
--

DROP TABLE IF EXISTS `team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `team_member` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_member`
--

LOCK TABLES `team_member` WRITE;
/*!40000 ALTER TABLE `team_member` DISABLE KEYS */;
INSERT INTO `team_member` VALUES (1,'Grace Githinji','Lawyer','team-1709475418.jpg');
/*!40000 ALTER TABLE `team_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Jane Doe','testimonial-1-1709474769.jpg','CEO, Kings Corporation','Kimotho Githinji & Company Advocates provided me with exceptional legal support. Their team navigated my complex corporate matters with ease, offering expert advice and personalized solutions. I highly recommend their services for anyone seeking reliable legal representation.'),(2,'John Kinuthia','testimonial-4-1709474811.jpg','Entrepreneur','As a client of Kimotho Githinji & Company Advocates, I experienced professionalism and integrity at every step. Their dedication to achieving favorable outcomes in disputes is commendable. I trust them implicitly with my legal matters'),(3,'Mary Johnson','testimonial-3-1709474857.jpg','Parent','I was impressed by the compassionate approach of Kimotho Githinji & Company Advocates in handling my family law case. They provided me with unwavering support and expert guidance throughout the process, ensuring my peace of mind during a challenging time.'),(4,'Naomi Wangui','testimonial-2-1709474899.jpg','Property Investor','Choosing Kimotho Githinji & Company Advocates was one of the best decisions I made for my real estate transactions. Their attention to detail and proactive guidance ensured smooth transactions and resolved disputes efficiently. I\'m grateful for their professionalism and expertise.');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `usertype` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `about` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'maestrojosiah@gmail.com','[\"ROLE_USER\", \"ROLE_ADMIN\"]','$2y$13$vgmwJL1.fjfHvIVX1BZ6.uU/td06ZMmzgO/EuRJyXYUbauHZ.TJ.e','Josiah','Birai',NULL,'a-neatly-designed-invoice-on-a-table-2-1709296341.png','admin',1,'Developer'),(2,'graceggithinji@gmail.com','[\"ROLE_USER\", \"ROLE_ADMIN\"]','0722804416','Grace','Githinji',NULL,NULL,'admin',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-04 21:40:50
